<?php
    // boolean start here

    $decision = true;
    if($decision){
        echo "the decision is True";
    }

    $decision = false;
    if($decision){
        echo "the decision is false";
    }
    // boolean end here

    // integer start here
    $value1 = 100;  //integer
    $value2 = 55.35; //float

    // integer end here

    // string start here

    $myString1 = 'abcd1234#$% $value1';
    $myString2 = "abcd1234#$% $value1";


    echo $myString1."<br>";
    echo $myString2."<br>";


    $heredocString =<<<BITM
            heredoc line1 $value1 <br>
            heredoc line3 $value1 <br>
            heredoc line3 $value1 <br>


BITM;
    echo $heredocString;

   $nowdocString =<<<'BITM'
            heredoc line1 $value1 <br>
            heredoc line3 $value1 <br>
            heredoc line3 $value1 <br>

BITM;

    echo $nowdocString;

    // string end here



// Array start here

    $ageMrA = 45;
    $ageMrB = 55;

    $indexArray = array(1,2,3,4,5,6,7);
    print_r($indexArray) ;
    echo "<br>";
    $indexArray = array("toyta", "bmw", "nisan", "ford", 3, 5.7);
    print_r($indexArray) ;


    $ageArray = array ("rahim"=>23, "Maynar ma"=>54, "Kuddos"=>36);
    echo "<pre>";
    print_r($ageArray);
    echo "</pre>";
    echo "<br>";
    echo ($ageArray['Maynar ma']);
    $ageArray['Maynar ma'] = 36;

    echo "<pre>";
    print_r($ageArray);
    echo "</pre>";

//Array end here


?>

